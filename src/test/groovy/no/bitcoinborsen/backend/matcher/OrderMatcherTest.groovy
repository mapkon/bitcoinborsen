package no.bitcoinborsen.backend.matcher

import org.junit.Test
import static no.bitcoinborsen.backend.Fixtures.*
import no.bitcoinborsen.model.*

class OrderMatcherTest {
    @Test
    void shouldBeNoMatchWhenBuyIsLowerThanSell() {
        BigDecimal buyPrice = 1.0g
        BigDecimal sellPrice = 2.0g
        BigDecimal amount = 10.0g

        BuyOrder buy = buyOrder(buyPrice, amount, new Date(2))
        SellOrder sell = sellOrder(sellPrice, amount, new Date(1))
        OrderMatcher matcher = new OrderMatcher(sell, buy)

        assert !matcher.isMatch()
    }

    @Test(expected = NoMatchException)
    void getTradeShouldThrowExceptionWhenThereIsNoMatch() {
        BigDecimal buyPrice = 1.0g
        BigDecimal sellPrice = 2.0g
        BigDecimal amount = 10.0g

        BuyOrder buy = buyOrder(buyPrice, amount, new Date(2))
        SellOrder sell = sellOrder(sellPrice, amount, new Date(1))
        OrderMatcher matcher = new OrderMatcher(sell, buy)

        matcher.getTrade()
    }

    @Test
    void shouldBeMatchWhenBuyPriceAndAmountIsEqualSellPriceAndAmount() {
        BigDecimal buyPrice = 1.0g
        BigDecimal sellPrice = 1.0g
        BigDecimal amount = 10.0g

        BuyOrder buy = buyOrder(buyPrice, amount, new Date(2))
        SellOrder sell = sellOrder(sellPrice, amount, new Date(1))
        OrderMatcher matcher = new OrderMatcher(sell, buy)

        assert matcher.isMatch()
    }

    @Test
    void shouldBeMatchWhenBuyLargerThanSell() {
        BuyOrder buy = buyOrder(2.0g, 1.0g, new Date(2))
        SellOrder sell = sellOrder(1.0g, 1.0g, new Date(1))
        OrderMatcher matcher = new OrderMatcher(sell, buy)

        assert matcher.isMatch()
    }

    @Test
    void testGetTradeWhenFullMatch() {
        BigDecimal price = 1.0g
        BigDecimal amount = 10.0g

        SellOrder sell = sellOrder(price, amount, new Date(1))
        BuyOrder buy = buyOrder(price, amount, new Date(2))
        OrderMatcher matcher = new OrderMatcher(sell, buy)

        Trade expected = trade(price, amount)

        assert matcher.getTrade(tradeId, tradeDate) == expected
        assert !matcher.hasRestSellOrder()
        assert !matcher.hasRestBuyOrder()
    }

    @Test
    void whenSellAmountIsLargerThanBuyAmountThenTradeAmountShouldEqualBuyAmount() {
        BigDecimal price = 1.0g
        BigDecimal sellAmount = 20.0g
        BigDecimal buyAmount = 5.0g

        SellOrder sell = sellOrder(price, sellAmount, new Date(1))
        BuyOrder buy = buyOrder(price, buyAmount, new Date(2))
        OrderMatcher matcher = new OrderMatcher(sell, buy)

        Trade expectedTrade = trade(price, buyAmount)

        assert matcher.getTrade(tradeId, tradeDate) == expectedTrade
        assert !matcher.hasRestBuyOrder()
        assert matcher.hasRestSellOrder()

        SellOrder expectedRestSellOrder = sellOrder(price, 15.0g, new Date(1))
        assert matcher.generateRestSellOrder() == expectedRestSellOrder
    }

    @Test
    void whenBuyAmountIsLargerThanSellAmountThenTradeAmountShouldEqualSellAmount() {
        BigDecimal price = 1.0g
        BigDecimal buyAmount = 20.0g
        BigDecimal sellAmount = 5.0g

        SellOrder sell = sellOrder(price, sellAmount, new Date(1))
        BuyOrder buy = buyOrder(price, buyAmount, new Date(2))
        OrderMatcher matcher = new OrderMatcher(sell, buy)

        Trade expectedTrade = trade(price, sellAmount)

        assert matcher.getTrade(tradeId, tradeDate) == expectedTrade
        assert !matcher.hasRestSellOrder()
        assert matcher.hasRestBuyOrder()

        BuyOrder expectedRestBuyOrder = buyOrder(price, 15.0g, new Date(2))
        assert matcher.generateRestBuyOrder() == expectedRestBuyOrder
    }

    @Test
    void selectTheFirstPostedPriceWhenPriceIsNotEqual() {
        BigDecimal sellPrice = 2.0g
        BigDecimal buyPrice = 3.0g
        BigDecimal amount = 10.0g

        SellOrder sell = sellOrder(sellPrice, amount, new Date(1))
        BuyOrder buy = buyOrder(buyPrice, amount, new Date(2))
        OrderMatcher matcher = new OrderMatcher(sell, buy)

        Trade expectedTrade = trade(sellPrice, amount)

        assert matcher.getTrade(tradeId, tradeDate) == expectedTrade
        assert !matcher.hasRestBuyOrder()
        assert !matcher.hasRestSellOrder()
    }

    @Test
    void select2TheFirstPostedPriceWhenPriceIsNotEqual() {
        BigDecimal sellPrice = 2.0g
        BigDecimal buyPrice = 3.0g
        BigDecimal amount = 10.0g

        SellOrder sell = sellOrder(sellPrice, amount, new Date(2))
        BuyOrder buy = buyOrder(buyPrice, amount, new Date(1))
        OrderMatcher matcher = new OrderMatcher(sell, buy)

        Trade expectedTrade = trade(buyPrice, amount)

        assert matcher.getTrade(tradeId, tradeDate) == expectedTrade
        assert !matcher.hasRestBuyOrder()
        assert !matcher.hasRestSellOrder()
    }

    @Test(expected = NoRestOrderException)
    void hasRestBuyOrderShouldThrowExceptionIfNoRestBuyOrder() {
        BigDecimal price = 1.0g
        BigDecimal sellAmount = 20.0g
        BigDecimal buyAmount = 5.0g

        SellOrder sell = sellOrder(price, sellAmount, new Date(1))
        BuyOrder buy = buyOrder(price, buyAmount, new Date(2))
        OrderMatcher matcher = new OrderMatcher(sell, buy)

        matcher.generateRestBuyOrder()
    }

    @Test(expected = NoRestOrderException)
    void hasRestSellOrderShouldThrowExceptionIfNoRestSellOrder() {
        BigDecimal price = 1.0g
        BigDecimal sellAmount = 5.0g
        BigDecimal buyAmount = 20.0g

        SellOrder sell = sellOrder(price, sellAmount, new Date(1))
        BuyOrder buy = buyOrder(price, buyAmount, new Date(2))
        OrderMatcher matcher = new OrderMatcher(sell, buy)

        matcher.generateRestSellOrder()
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenBuyOrderIsNull() {
        new OrderMatcher(sellOrder1, null)
    }

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenSellOrderIsNull() {
        new OrderMatcher(null, buyOrder1)
    }
}
