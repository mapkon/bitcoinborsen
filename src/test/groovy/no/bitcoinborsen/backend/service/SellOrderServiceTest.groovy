package no.bitcoinborsen.backend.service

import no.bitcoinborsen.backend.dao.NoOrderFoundException
import no.bitcoinborsen.backend.dao.OrderDao
import no.bitcoinborsen.backend.util.DateGenerator
import no.bitcoinborsen.backend.util.IdGenerator

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import static no.bitcoinborsen.backend.Fixtures.*
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class SellOrderServiceTest {
    @Mock
    OrderDao dao
    @Mock
    DateGenerator dateGenerator
    @Mock
    IdGenerator idGenerator
    @Mock
    TradeService tradeService
    @Mock
    BalanceService bitcoinBalance
    @InjectMocks
    SellOrderService sellOrderService

    @Before
    void configureMocks() {
        when(dateGenerator.now()).thenReturn(tradeDate)
        when(idGenerator.random()).thenReturn(tradeId)
        when(bitcoinBalance.getSpendable(any(UUID))).thenReturn(999999.99g)

        sellOrderService.bitcoinBalance = bitcoinBalance
    }

    @After
    void verifyGetBalanceNeverCalled() {
        verify(bitcoinBalance, never()).getBalance(any(UUID))
    }

    private void verifyNoOtherUpdateInteractions() {
        verify(dao, atLeast(0)).getLowestSellOrder()
        verify(dao, atLeast(0)).getHighestBuyOrder()
        verifyNoMoreInteractions(dao, tradeService)
    }

    @Test
    void whenNoMatchThenOrderShouldJustBeAddedWhenNewSellOrder() {
        def highestBuyOrder = buyOrder(5.0g, 1.0g, new Date(1))
        def newSellOrder = sellOrder(10.0g, 1.0g, new Date(10))

        when(dao.getHighestBuyOrder()).thenReturn(highestBuyOrder)

        sellOrderService.newOrder(newSellOrder)

        verify(dao).insertSellOrder(eq(newSellOrder))
        verifyNoOtherUpdateInteractions()
    }

    @Test
    void whenNoBuyOrdersThenOrderShouldJustBeAddedWhenNewSellOrder() {
        def newSellOrder = sellOrder(10.0g, 1.0g, new Date(10))

        when(dao.getHighestBuyOrder()).thenThrow(new NoOrderFoundException())

        sellOrderService.newOrder(newSellOrder)

        verify(dao).insertSellOrder(eq(newSellOrder))
        verifyNoOtherUpdateInteractions()
    }

    @Test
    void newSellOrderFillHalfOfExsistingBuyOrder() {
        def highestBuyOrder = buyOrder(11.0g, 7.0g, new Date(1))
        def newSellOrder = sellOrder(10.0g, 1.0g, new Date(10))

        when(dao.getHighestBuyOrder()).thenReturn(highestBuyOrder)

        sellOrderService.newOrder(newSellOrder)

        def restBuyOrder = buyOrder(11.0g, 6.0g, new Date(1))
        def expectedTrade = trade(11.0g, 1.0g)
        verify(dao).deleteBuyOrder(eq(highestBuyOrder.id))
        verify(dao).insertBuyOrder(eq(restBuyOrder))
        verify(tradeService).newTrade(eq(expectedTrade))
        verifyNoOtherUpdateInteractions()
    }

    @Test
    void newSellOrderFillExsistingBuyOrderExact() {
        def highestBuyOrder = buyOrder(11.0g, 7.0g, new Date(1))
        def newSellOrder = sellOrder(11.0g, 7.0g, new Date(10))

        when(dao.getHighestBuyOrder()).thenReturn(highestBuyOrder)

        sellOrderService.newOrder(newSellOrder)

        def expectedTrade = trade(11.0g, 7.0g)
        verify(dao).deleteBuyOrder(highestBuyOrder.id)
        verify(tradeService).newTrade(eq(expectedTrade))
        verifyNoOtherUpdateInteractions()
    }

    @Test
    void newSellOrderFillMoreThanExsistingBuyOrder() {
        def highestBuyOrder = buyOrder(11.0g, 7.0g, new Date(1))
        def newSellOrder = sellOrder(9.0g, 17.0g, new Date(10))

        when(dao.getHighestBuyOrder()).thenReturn(highestBuyOrder).thenThrow(new NoOrderFoundException())

        sellOrderService.newOrder(newSellOrder)

        def expectedTrade = trade(11.0g, 7.0g)
        def restSellOrder = sellOrder(9.0g, 10.0g, new Date(10))
        verify(dao).deleteBuyOrder(highestBuyOrder.id)
        verify(dao).insertSellOrder(eq(restSellOrder))
        verify(tradeService).newTrade(eq(expectedTrade))
        verifyNoOtherUpdateInteractions()
    }

    @Test(expected = NotEnoughFoundsException)
    void cantCreateSellOrderWithoutEnoughBitcoins() {
        when(bitcoinBalance.getSpendable(eq(seller.id))).thenReturn(9.99980g)

        def newSellOrder = sellOrder(9.0g, 10.0g, new Date(10))
        try {
            sellOrderService.newOrder(newSellOrder)
        } finally {
            verifyNoOtherUpdateInteractions()
        }
    }

    @Test
    void shouldBeAllowedToCreateSellOrderWithExactlyTheSameAmountAsBalance() {
        def highestBuyOrder = buyOrder(5.0g, 1.0g, new Date(1))
        def newSellOrder = sellOrder(10.0g, 10.0g, new Date(10))

        when(bitcoinBalance.getSpendable(eq(seller.id))).thenReturn(100.00g)
        when(dao.getHighestBuyOrder()).thenReturn(highestBuyOrder)

        sellOrderService.newOrder(newSellOrder)

        verify(dao).insertSellOrder(eq(newSellOrder))
        verifyNoOtherUpdateInteractions()
    }

    @Test
    void deleteSellOrderShouldJustForwardToDao() {
        sellOrderService.deleteOrder(sellOrderId)
        verify(dao).deleteSellOrder(sellOrderId)
    }
}
