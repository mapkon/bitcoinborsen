package no.bitcoinborsen.backend.service

import no.bitcoinborsen.backend.dao.TraderDao
import no.bitcoinborsen.backend.dao.TraderNotFoundException
import no.bitcoinborsen.backend.util.AccountName
import no.bitcoinborsen.backend.util.BankKid
import no.bitcoinborsen.backend.util.IdGenerator
import no.bitcoinborsen.model.Trader
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import ru.paradoxs.bitcoin.client.BitcoinClient
import static no.bitcoinborsen.backend.Fixtures.seller
import static org.mockito.Mockito.*

@RunWith(MockitoJUnitRunner)
class TraderServiceImplTest {
    @Mock
    TraderDao dao
    @Mock
    IdGenerator generator
    @Mock
    BitcoinClient bitcoinClient
    @Mock
    AccountName accountName
    @Mock
    BankKid bankKid
    @InjectMocks
    TraderServiceImpl service

    @Test
    void shouldCreateTraderOnFirstLogin() {
        when(generator.random()).thenReturn(seller.id)
        when(dao.getTraderByOpenId(anyString())).thenThrow(new TraderNotFoundException())
        when(bitcoinClient.getAccountAddress(anyString())).thenReturn(seller.getBitcoinAddress())
        when(accountName.fromOpenId(anyString())).thenReturn("dummy")
        when(bankKid.fromId(any(UUID))).thenReturn(seller.bankKid)

        service.haveBeenAuthenticated(seller.openId, seller.email)

        verify(dao).newTrader(eq(seller))
    }

    @Test
    void shouldNotCreateTraderOnSecondLogin() {
        when(dao.getTraderByOpenId(anyString())).thenReturn(seller)

        service.haveBeenAuthenticated(seller.openId, seller.email)

        verify(dao, never()).newTrader(any(Trader))
    }

    @Test
    void getTraderByKidNumberShouldJustCallDao() {
        when(dao.getByKidNumber(eq(seller.bankKid))).thenReturn(seller)

        assert service.getFromKidNumber(seller.bankKid) == seller
    }
}
