package no.bitcoinborsen.backend.dao

import no.bitcoinborsen.model.Trade
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import static no.bitcoinborsen.backend.Fixtures.*

class JdbcTradeDaoIntegrationTest extends JdbcIntegrationTestBase {
    @Autowired
    JdbcTradeDao tradeDao

    @Test
    void insertTrade() {
        long count = tradeDao.getTradeCount()

        Trade trade = trade(10.0g, 5.0g)
        tradeDao.insertTrade(trade)

        assert tradeDao.getTradeCount() == count + 1
    }

    @Test(expected = Exception)
    void insertTradeWithInvalidBuyerShouldThrowException() {
        Trade trade = new Trade(10.0g, 5.0g, seller.id, unknownTrader.id)
        tradeDao.insertTrade(trade)
    }

    @Test(expected = Exception)
    void insertTradeWithInvalidSellerShouldThrowException() {
        Trade trade = new Trade(10.0g, 5.0g, unknownTrader.id, buyer.id)
        tradeDao.insertTrade(trade)
    }

    @Test
    void buyerShouldHave3BuyTrades() {
        def trades = tradeDao.getBuyesForTrader(buyer.id)

        assert trades.size() == 3
        assert trades.containsAll([trade1, trade2, trade3])
    }

    @Test
    void sellerShouldHaveZeroBuyTrades() {
        def trades = tradeDao.getBuyesForTrader(seller.id)
        assert trades.isEmpty()
    }

    @Test
    void sellerShouldHave3SellTrades() {
        def trades = tradeDao.getSalesForTrader(seller.id)

        assert trades.size() == 3
        assert trades.containsAll([trade1, trade2, trade3])
    }

    @Test
    void buyerShouldHaveZeroSellTrades() {
        def trades = tradeDao.getSalesForTrader(buyer.id)
        assert trades.isEmpty()
    }

    @Test
    void listSinceTimeZeroShouldIncludeEveryTrade() {
        def trades = tradeDao.list(new Date(0L))

        assert trades.size() == 3
        assert trades.containsAll([trade1, trade2, trade3])
    }

    @Test
    void listSinceTimeEndShouldIncludeNoTrade() {
        def trades = tradeDao.list(new Date(Long.MAX_VALUE))
        assert trades.isEmpty()
    }

    @Test
    void listShouldIncludeTradesOnThatDate() {
        def trades = tradeDao.list(trade3.traded)
        assert trades == [trade3]
    }
}
