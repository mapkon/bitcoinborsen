package no.bitcoinborsen.backend.dao

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.jdbc.core.JdbcOperations

@RunWith(MockitoJUnitRunner)
class JdbcOrderDaoTest {
    @Mock
    private JdbcOperations jdbcTemplate
    @InjectMocks
    private JdbcOrderDao orderDao

    @Test(expected = NoOrderFoundException)
    void gettingLowestSellOrderOnEmptyOrderbookShouldThrowException() {
        orderDao.getLowestSellOrder()
    }

    @Test(expected = NoOrderFoundException)
    void gettingHighestBuyOrderOnEmptyOrderbookShouldThrowException() {
        orderDao.getHighestBuyOrder()
    }
}
