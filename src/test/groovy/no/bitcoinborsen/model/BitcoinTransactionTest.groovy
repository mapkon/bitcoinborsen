package no.bitcoinborsen.model

import org.junit.Test
import static no.bitcoinborsen.backend.Fixtures.bitcoinTransaction

class BitcoinTransactionTest {

    @Test(expected = AssertionError)
    void shouldThrowExceptionWhenAmountIsZero() {
        bitcoinTransaction(0.0g)
    }

    @Test
    void isDeposit() {
        BitcoinTransaction transaction = bitcoinTransaction(10.0g)

        assert transaction.isDeposit()
        assert !transaction.isWithdraw()
    }

    @Test
    void isWithdraw() {
        BitcoinTransaction transaction = bitcoinTransaction(-122.2g)

        assert transaction.isWithdraw()
        assert !transaction.isDeposit()
    }

    @Test
    void sameAmountButDifferentFormatShouldBeEqual() {
        BitcoinTransaction a = bitcoinTransaction(10.00000100g)
        BitcoinTransaction b = bitcoinTransaction(new BigDecimal("10.000001"))

        assert a == b
        assert a.hashCode() == b.hashCode()
    }
}
