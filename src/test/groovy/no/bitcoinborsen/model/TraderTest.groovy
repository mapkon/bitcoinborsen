package no.bitcoinborsen.model

import org.junit.Test

class TraderTest {
    static final UUID id = UUID.fromString("5b05e29d-5fed-45fc-8ff8-9550d1eeca17")
    static final String openId = "http://openid.test.com"
    static final String email = "dummy@test.com"
    static final String address = "mhwnQwTbDeqAMuTcWa4k5987VuKo64FtCh"
    static final String kid = "12345678901234567890123"

    @Test
    void test() {
        new Trader(id, openId, email, address, kid)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenIdIsNull() {
        new Trader(null, openId, email, address, kid)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenOpenIdIsNull() {
        new Trader(id, null, email, address, kid)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenEmailIsNull() {
        new Trader(id, openId, null, address, kid)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenBitcoinAddressIsNull() {
        new Trader(id, openId, email, null, kid)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenBitcoinAddressIsTooShort() {
        String badAddress = "xxxxxxx"
        new Trader(id, openId, email, badAddress, kid)
    }

    @Test(expected = AssertionError)
    void shouldThrowThrowableWhenKidIsTooLong() {
        String badKid = "99999999999999999999999999999999999999999999999999"
        new Trader(id, openId, email, address, badKid)
    }
}
