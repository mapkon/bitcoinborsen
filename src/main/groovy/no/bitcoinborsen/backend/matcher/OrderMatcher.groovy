package no.bitcoinborsen.backend.matcher

import no.bitcoinborsen.backend.util.*
import no.bitcoinborsen.model.*

final class OrderMatcher {
    private final SellOrder sellOrder
    private final BuyOrder buyOrder
    DateGenerator dateGenerator = new DefaultDateGenerator()
    IdGenerator idGenerator = new DefaultIdGenerator()

    OrderMatcher(SellOrder sellOrder, BuyOrder buyOrder) {
        assert sellOrder != null
        assert buyOrder != null

        this.sellOrder = sellOrder
        this.buyOrder = buyOrder
    }

    private BigDecimal getTradePrice() {
        if (sellOrder.placed < buyOrder.placed) {
            sellOrder.price
        } else {
            buyOrder.price
        }
    }

    private BigDecimal getTradeAmount() {
        sellOrder.amount.min(buyOrder.amount)
    }

    boolean isMatch() {
        sellOrder.price <= buyOrder.price
    }

    Trade getTrade() {
        getTrade(idGenerator.random(), dateGenerator.now())
    }

    Trade getTrade(UUID tradeId, Date traded) {
        if (!isMatch()) {
            throw new NoMatchException()
        }

        BigDecimal price = getTradePrice()
        BigDecimal amount = getTradeAmount()

        new Trade(price, amount, sellOrder.seller, buyOrder.buyer, traded, tradeId)
    }

    boolean hasRestSellOrder() {
        buyOrder.amount < sellOrder.amount
    }

    boolean hasRestBuyOrder() {
        sellOrder.amount < buyOrder.amount
    }

    SellOrder generateRestSellOrder() {
        if (!hasRestSellOrder()) {
            throw new NoRestOrderException()
        }

        BigDecimal restAmount = sellOrder.amount - buyOrder.amount
        new SellOrder(sellOrder.price, restAmount, sellOrder.seller, sellOrder.placed, sellOrder.id)
    }

    BuyOrder generateRestBuyOrder() {
        if (!hasRestBuyOrder()) {
            throw new NoRestOrderException()
        }

        BigDecimal restAmount = buyOrder.amount - sellOrder.amount
        new BuyOrder(buyOrder.price, restAmount, buyOrder.buyer, buyOrder.placed, buyOrder.id)
    }
}
