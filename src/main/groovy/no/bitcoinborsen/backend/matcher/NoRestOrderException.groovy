package no.bitcoinborsen.backend.matcher

import groovy.transform.InheritConstructors

@InheritConstructors
final class NoRestOrderException extends RuntimeException {
    private static final serialVersionUID = 2L
}
