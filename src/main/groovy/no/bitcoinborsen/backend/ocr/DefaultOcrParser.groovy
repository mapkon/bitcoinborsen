package no.bitcoinborsen.backend.ocr

import org.springframework.stereotype.Component

@Component
final class DefaultOcrParser implements OcrParser {
    @Override
    Collection<OcrTransaction> listTransactions(InputStream input) {
        def transactions = []
        String mailNumber = null
        String missionNumber = null
        String line1 = null
        input.eachLine { line ->
            if (line.startsWith('NY000010')) {
                mailNumber = line[16..22]
            } else if (line.startsWith('NY090020')) {
                missionNumber = line[17..23]
            } else if (line.startsWith('NY09') && line[6..7] == '30') {
                line1 = line
            } else if (line.startsWith('NY09') && line[6..7] == '31') {
                transactions << newDeposit(line1, line, mailNumber, missionNumber)
            }
        }

        return transactions
    }

    private OcrTransaction newDeposit(String line1, String line2, String mailNumber, String missionNumber) {
        String transactionNumber = line1[8..14]
        String amountMultipliedWith100 = line1[31..48]
        String kid = line1[49..73].trim()
        String accountNumber = line2[47..57]
        Date transfered = Date.parse('ddMMyy', line2[41..46])

        String id = mailNumber + missionNumber + transactionNumber
        BigDecimal amount = amountMultipliedWith100.toBigDecimal() / 100

        new OcrTransaction(id, kid, amount, accountNumber, transfered)
    }
}
