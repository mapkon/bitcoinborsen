package no.bitcoinborsen.backend.util

interface IdGenerator {
    UUID random()
}
