package no.bitcoinborsen.backend.util

interface AccountName {
    String fromOpenId(String openId)
}
