package no.bitcoinborsen.backend.dao

import java.sql.ResultSet
import no.bitcoinborsen.model.BitcoinTransaction
import no.bitcoinborsen.model.TransactionStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.IncorrectResultSizeDataAccessException
import org.springframework.jdbc.core.JdbcOperations
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository

@Repository("bitcoinTransactionDao")
final class JdbcBitcoinTransactionDao implements BitcoinTransactionDao {
    @Autowired
    JdbcOperations jdbcTemplate

    private static final SELECT_FIELDS = "select id, trader, amount, address, placed, transaction_hash, status"

    @Override
    Collection<BitcoinTransaction> getByTrader(UUID trader) {
        String sql = "${SELECT_FIELDS} from bitcoin_transaction where trader=?"
        return jdbcTemplate.query(sql, new BitcoinTransactionRowMapper(), trader)
    }

    @Override
    BitcoinTransaction getById(UUID id) {
        String sql = "${SELECT_FIELDS} from bitcoin_transaction where id=?"
        try {
            return jdbcTemplate.queryForObject(sql, new BitcoinTransactionRowMapper(), id)
        } catch (IncorrectResultSizeDataAccessException ex) {
            throw new TransactionNotFoundException(ex)
        }
    }

    @Override
    BitcoinTransaction getByTransactionHash(String hash) {
        String sql = "${SELECT_FIELDS} from bitcoin_transaction where transaction_hash=?"
        try {
            return jdbcTemplate.queryForObject(sql, new BitcoinTransactionRowMapper(), hash)
        } catch (IncorrectResultSizeDataAccessException ex) {
            throw new TransactionNotFoundException(ex)
        }
    }

    @Override
    Collection<BitcoinTransaction> getQueuedWithdrawals() {
        String sql = "${SELECT_FIELDS} from bitcoin_transaction where status='Q'"
        return jdbcTemplate.query(sql, new BitcoinTransactionRowMapper())
    }

    @Override
    void insert(BitcoinTransaction transaction) {
        String sql = '''insert into bitcoin_transaction(id, trader, amount, address, placed, transaction_hash, status)
                        values(?, ?, ?, ?, ?, ?, ?)'''

        jdbcTemplate.update(sql, transaction.id,
                transaction.trader,
                transaction.amount,
                transaction.address,
                transaction.placed,
                transaction.transactionHash,
                transaction.status.toChar())
    }

    @Override
    void updateStatus(UUID id, TransactionStatus status) {
        String sql = "update bitcoin_transaction set status=? where id=?"
        jdbcTemplate.update(sql, status.toChar(), id)
    }

    @Override
    void markAsFinished(UUID id, String hash) {
        String sql = "update bitcoin_transaction set transaction_hash=?, status='F' where id=?"
        jdbcTemplate.update(sql, hash, id)
    }

    @Override
    long getTotalCount() {
        String sql = "select count(*) from bitcoin_transaction"
        return jdbcTemplate.queryForLong(sql)
    }
}

private final class BitcoinTransactionRowMapper implements RowMapper<BitcoinTransaction> {
    @Override
    BitcoinTransaction mapRow(ResultSet rs, int i) {
        def id = UUID.fromString(rs.getString("id"))
        def trader = UUID.fromString(rs.getString("trader"))
        def amount = rs.getBigDecimal("amount")
        def address = rs.getString("address")
        def placed = rs.getTimestamp("placed")
        def transactionHash = rs.getString("transaction_hash")
        def status = TransactionStatus.fromChar(rs.getString("status"))

        new BitcoinTransaction(id, trader, amount, address, placed, transactionHash, status)
    }
}
