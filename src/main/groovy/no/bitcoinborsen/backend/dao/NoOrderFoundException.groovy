package no.bitcoinborsen.backend.dao

final class NoOrderFoundException extends Exception {

    private static final serialVersionUID = 1L

    NoOrderFoundException() {
        super()
    }
}
