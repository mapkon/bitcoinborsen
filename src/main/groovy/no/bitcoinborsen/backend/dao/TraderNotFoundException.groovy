package no.bitcoinborsen.backend.dao

class TraderNotFoundException extends RuntimeException {

    private static final serialVersionUID = 1L

    TraderNotFoundException() {
        super()
    }

    TraderNotFoundException(Throwable throwable) {
        super(throwable)
    }
}
