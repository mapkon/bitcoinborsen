package no.bitcoinborsen.backend.dao

import java.sql.ResultSet
import no.bitcoinborsen.model.BankTransaction
import no.bitcoinborsen.model.TransactionStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcOperations
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Repository

@Repository("bankTransactionDao")
final class JdbcBankTransactionDao implements BankTransactionDao {
    @Autowired
    JdbcOperations jdbcTemplate

    @Override
    void insert(BankTransaction transaction) {
        def sql = '''insert into bank_transaction(id, trader, amount, account_number, transfered, transaction_id, status)
                        values(?, ?, ?, ?, ?, ?, ?)'''

        jdbcTemplate.update(sql, transaction.id,
                transaction.trader,
                transaction.amount,
                transaction.accountNumber,
                transaction.transfered,
                transaction.transactionId,
                transaction.status.toChar())
    }

    @Override
    BankTransaction getByTransactionId(String transactionId) {
        def sql = 'select * from bank_transaction where transaction_id=?;'
        try {
            jdbcTemplate.queryForObject(sql, new BankTransactionRowMapper(), transactionId)
        } catch (EmptyResultDataAccessException) {
            throw new TransactionNotFoundException("Transaction id: ${transactionId}")
        }
    }

    @Override
    void updateStatus(UUID id, TransactionStatus newStatus) {
        def sql = 'update bank_transaction set status=? where id=?;'
        jdbcTemplate.update(sql, newStatus.toChar(), id)
    }

    @Override
    Collection<BankTransaction> getByTrader(UUID trader) {
        def sql = 'select * from bank_transaction where trader=?;'
        jdbcTemplate.query(sql, new BankTransactionRowMapper(), trader)
    }

    @Override
    long getTotalCount() {
        def sql = 'select count(*) from bank_transaction;'
        jdbcTemplate.queryForLong(sql)
    }

    @Override
    BankTransaction getById(UUID id) {
        def sql = 'select * from bank_transaction where id=?;'
        try {
            return jdbcTemplate.queryForObject(sql, new BankTransactionRowMapper(), id)
        } catch (EmptyResultDataAccessException) {
            throw new TransactionNotFoundException("Id: ${id}")
        }
    }
}

private final class BankTransactionRowMapper implements RowMapper<BankTransaction> {
    @Override
    BankTransaction mapRow(ResultSet rs, int i) {
        def id = UUID.fromString(rs.getString("id"))
        def trader = UUID.fromString(rs.getString("trader"))
        def amount = rs.getBigDecimal("amount")
        def address = rs.getString("account_number")
        def transfered = rs.getTimestamp("transfered")
        def transactionId = rs.getString("transaction_id")
        def status = TransactionStatus.fromChar(rs.getString("status"))

        new BankTransaction(id, trader, amount, address, transfered, transactionId, status)
    }
}