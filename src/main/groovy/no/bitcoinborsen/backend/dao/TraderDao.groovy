package no.bitcoinborsen.backend.dao


import no.bitcoinborsen.model.Trader

interface TraderDao {
    Trader getTraderByOpenId(String username)

    void newTrader(Trader newTrader)

    Collection<Trader> listAllTraders()

    Trader getByKidNumber(String kidNumber)
}
