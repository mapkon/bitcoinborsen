package no.bitcoinborsen.frontend.forms

import groovy.transform.Canonical

@Canonical
class NewOrderForm {
    BigDecimal price
    BigDecimal amount
}
