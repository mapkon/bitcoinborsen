package no.bitcoinborsen.frontend.bitcoincharts

import groovy.json.StreamingJsonBuilder
import no.bitcoinborsen.backend.service.TradeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@Controller
@RequestMapping('/bitcoincharts/trades.json')
class Trades {
    @Autowired
    TradeService tradeService
    private static final DAYS_OF_TRADES = 5

    @RequestMapping(method = RequestMethod.GET, produces = 'application/json')
    void list(Writer response) {
        def json = new StreamingJsonBuilder(response)
        json(
                tradeService.list(DAYS_OF_TRADES).collect { trade ->
                    def unixtime = trade.traded.time
                    [
                            date: unixtime / 1000L,
                            price: trade.price,
                            amount: trade.amount,
                            tid: unixtime
                    ]
                }
        )
    }
}
