package no.bitcoinborsen.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import java.math.RoundingMode

@EqualsAndHashCode
@ToString(includeNames = true)
final class Trade implements Serializable {
    private static final serialVersionUID = 3L
    final UUID id
    final BigDecimal price
    final BigDecimal amount
    final UUID seller
    final UUID buyer
    final Date traded

    Trade(BigDecimal price, BigDecimal amount, UUID seller, UUID buyer) {
        this(price, amount, seller, buyer, new Date(), UUID.randomUUID())
    }

    Trade(BigDecimal price, BigDecimal amount, UUID seller, UUID buyer, Date traded, UUID id) {
        assert id != null
        assert price != null && price > 0.0g
        assert amount != null && amount > 0.0g
        assert seller != null
        assert buyer != null
        assert traded != null

        this.id = id
        this.price = price.setScale(2, RoundingMode.HALF_UP)
        this.amount = amount.setScale(8, RoundingMode.HALF_UP)
        this.seller = seller
        this.buyer = buyer
        this.traded = new Date(traded.getTime())
    }

    Date getTraded() {
        new Date(traded.getTime())
    }

    BigDecimal getTotal() {
        BigDecimal total = amount * price
        total.setScale(2, RoundingMode.HALF_UP)
    }
}
