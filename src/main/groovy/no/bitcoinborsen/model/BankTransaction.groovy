package no.bitcoinborsen.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import java.math.RoundingMode

@EqualsAndHashCode
@ToString(includeNames = true)
final class BankTransaction implements Serializable {
    private static final serialVersionUID = 7L
    private static final ACCOUNT_NUMBER_LENGTH = 11
    private static final TRANSACTION_ID_LENGTH = 21

    final UUID id
    final UUID trader
    final BigDecimal amount
    final String accountNumber
    final Date transfered
    final TransactionStatus status
    final String transactionId

    BankTransaction(UUID id, UUID trader, BigDecimal amount, String accountNumber, Date transfered, String transactionId, TransactionStatus status) {
        assert id != null
        assert trader != null
        assert amount != null && amount != 0.0g
        assert accountNumber != null && accountNumber.length() == ACCOUNT_NUMBER_LENGTH
        assert transfered != null
        assert transactionId != null && transactionId.length() == TRANSACTION_ID_LENGTH
        assert status != null

        this.id = id
        this.trader = trader
        this.amount = amount.setScale(2, RoundingMode.HALF_UP)
        this.accountNumber = accountNumber
        this.transfered = new Date(transfered.time)
        this.transactionId = transactionId
        this.status = status
    }

    Date getTransfered() {
        new Date(transfered.time)
    }

    Boolean isDeposit() {
        amount > 0.0g
    }

    Boolean isWithdraw() {
        amount < 0.0g
    }
}
