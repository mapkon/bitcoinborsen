package no.bitcoinborsen.security

import org.slf4j.MDC
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import javax.servlet.*

final class AddUserToMdcFilter implements Filter {
    @Override
    void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {
        def user = SecurityContextHolder.context.authentication.principal
        if (user instanceof UserDetails) {
            user = user.getUsername()
        }

        MDC.put("user", user)
        try {
            chain.doFilter(request, response)
        } finally {
            MDC.remove("user")
        }
    }

    @Override
    void init(FilterConfig filterConfig) {}

    @Override
    void destroy() {}
}
