package no.bitcoinborsen.security

import groovy.util.logging.Slf4j
import no.bitcoinborsen.backend.service.TraderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import org.springframework.security.core.userdetails.*

@Slf4j
@Component("userService")
class AuthenticateAllUsers implements UserDetailsService {
    private static final TRADER = new SimpleGrantedAuthority("ROLE_USER")
    private static final ADMIN = new SimpleGrantedAuthority("ROLE_ADMIN")

    @Value('${admin.username}')
    String admin
    @Autowired
    TraderService traderService

    @Override
    UserDetails loadUserByUsername(String username) {
        traderService.haveBeenAuthenticated(username, "todo@email.com")
        if (username.equals(admin)) {
            log.info("Admin '${username}' logged in")
            new User(username, "", [TRADER, ADMIN])
        } else {
            log.info("Trader '${username}' logged in")
            new User(username, "", [TRADER])
        }
    }
}
