/**
 * Copyright 2010 Aleksey Krivosheev (paradoxs.mail@gmail.com)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License")
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package ru.paradoxs.bitcoin.client

import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import no.bitcoinborsen.backend.util.DefaultIdGenerator
import no.bitcoinborsen.backend.util.IdGenerator
import org.json.JSONArray
import org.json.JSONObject
import ru.paradoxs.bitcoin.http.HttpSession
import ru.paradoxs.bitcoin.http.Session
import org.apache.commons.httpclient.*

/**
 * A Java API for accessing a Bitcoin server.
 *
 * PLEASE NOTE: It doesn't use https for the communication, just http, but access to the Bitcoin server is
 * only allowed from localhost, so it shouldn't really matter.
 *
 * TODO: The API should really use BigDecimal instead of doubles.
 *
 * @see <a href="http://www.bitcoin.org/wiki/doku.php?id=api">Bitcoin API</a>
 * @author paradoxs
 * @author mats@henricson.se
 */
class JsonRpcBitcoinClient implements BitcoinClient {

    Session session = null
    IdGenerator idGenerator = new DefaultIdGenerator()

    JsonRpcBitcoinClient() {}

    /**
     * Creates a BitcoinClient
     *
     * @param host the host machine where there's an executing bitcoind server
     * @param login the username to access the bitcoind server
     * @param password the password to access the bitcoind server
     * @param port the port number to the bitcoind server
     */
    JsonRpcBitcoinClient(String host, String login, String password, int port = 8332) {
        Credentials credentials = new UsernamePasswordCredentials(login, password)
        URI uri = new URI("http", null, host, port, null, null, null)
        session = new HttpSession(uri, credentials)
    }

    /**
     * Returns the list of addresses for the given account
     *
     * @param account name of account, if null or empty it means the default account
     * @return list of addresses for the given account
     * @since 0.3.18
     */
    @Override
    List<String> getAddressesByAccount(String account) {
        rpc('getaddressesbyaccount', [account ?: ''])
    }

    /**
     * Returns the available balance for the default account
     *
     * @return the balance for the default account
     */
    @Override
    double getBalance() {
        rpcOld('getbalance')
    }

    /**
     * Returns the available balance for an account
     *
     * @param account the name of the account, or the default account if null or empty
     * @return the balance
     * @since 0.3.18
     */
    @Override
    double getBalance(String account) {
        rpcOld('getbalance', [account ?: ''])
    }

    /**
     * Returns the number of blocks in the longest block chain
     *
     * @return the number of blocks
     */
    @Override
    int getBlockCount() {
        rpc('getblockcount')
    }

    /**
     * Returns the block number of the latest block in the longest block chain
     *
     * @return the block number
     */
    @Override
    int getBlockNumber() {
        rpc('getblocknumber')
    }

    /**
     * Returns the number of connections to other nodes
     *
     * @return the number of connections
     */
    @Override
    int getConnectionCount() {
        rpc('getconnectioncount')
    }

    /**
     * Returns the current number of hashes computed per second
     *
     * @return the number of computed hashes per second
     * @since 0.3.18
     */
    @Override
    long getHashesPerSecond() {
        rpc('gethashespersec')
    }

    /**
     * Returns the proof-of-work difficulty as a multiple of the minimum difficulty
     *
     * @return the current difficulty
     */
    @Override
    double getDifficulty() {
        rpcOld('getdifficulty')
    }

    /**
     * Returns boolean true if server is trying to generate bitcoins, false otherwise
     *
     * @return true if server is trying to generate bitcoins, false otherwise
     */
    @Override
    boolean getGenerate() {
        rpc('getgenerate')
    }

    /**
     * Turn on/off coins generation
     *
     * @param isGenerate on - true, off - false
     * @param processorsCount proccesorsCount processors, -1 is unlimited
     */
    @Override
    void setGenerate(boolean isGenerate, int processorsCount) {
        rpc('setgenerate', [isGenerate, processorsCount])
    }

    /**
     * Return server information, about balance, connections, blocks...etc.
     *
     * @return server information
     */
    @Override
    ServerInfo getServerInfo() {
        def result = rpcOld('getinfo')

        new ServerInfo(
                balance: result.balance,
                blocks: result.blocks,
                connections: result.connections,
                difficulty: result.difficulty,
                hashesPerSecond: result.hashespersec,
                generate: result.generate,
                usedCPUs: result.genproclimit,
                version: result.version,
                proxy: result.proxy,
                testnet: result.testnet
        )
    }

    /**
     * Returns the account associated with the given address
     *
     * @param address the address for which we want to lookup the account
     * @return the account associated with a certain address
     * @since 0.3.18
     */
    @Override
    String getAccount(String address) {
        rpc('getaccount', [address])
    }

    /**
     * Sets the account associated with the given address, or removes the address if the account is null
     *
     * @param address the address to be added or removed
     * @param account if null then address is removed
     * @since 0.3.18
     */
    @Override
    void setAccountForAddress(String address, String account) {
        rpc('setaccount', [address, account])
    }

    /**
     * Returns a new bitcoin address for receiving payments. If account is null
     * or an empty string, then that means the default account.
     *
     * @param account the name of the account for which we want a new receiving address.
     *                The address can be null or the empty string, which means the default account.
     * @return the new bitcoin address for receiving payments to this account
     * @since 0.3.18
     */
    @Override
    String getAccountAddress(String account) {
        rpc('getaccountaddress', [account ?: ''])
    }

    /**
     * Returns the total amount received by bitcoinaddress in transactions
     *
     * @param address the address we want to know the received amount for
     * @param minimumConfirmations the minimum number of confirmations for a transaction to count
     * @return total amount received
     */
    @Override
    double getReceivedByAddress(String address, long minimumConfirmations) {
        rpcOld('getreceivedbyaddress', [address, minimumConfirmations])
    }

    /**
     * Returns the total amount received by addresses for account in transactions
     *
     * @param account the name of the account, or the default account if null or empty string
     * @param minimumConfirmations minimum number of confirmations for the transaction to be included in the received amount
     * @return total amount received for this account
     * @since 0.3.18
     */
    @Override
    double getReceivedByAccount(String account, long minimumConfirmations) {
        rpcOld('getreceivedbyaccount', [account, minimumConfirmations])
    }

    /**
     * Return help for a command
     *
     * @param command the command
     * @return the help text
     */
    @Override
    String help(String command) {
        rpc('help', [command])
    }

    /**
     * Info about all received transactions by address
     *
     * @param minimumConfirmations is the minimum number of confirmations before payments are included
     * @param includeEmpty whether to include addresses that haven't received any payments
     * @return info about all received transactions by address
     */
    @Override
    List<AddressInfo> listReceivedByAddress(long minimumConfirmations, boolean includeEmpty) {
        JSONArray result = rpcOld('listreceivedbyaddress', [minimumConfirmations, includeEmpty])
        int size = result.length()
        List<AddressInfo> list = []

        for (int i = 0; i < size; i++) {
            def jObject = result.getJSONObject(i)
            AddressInfo info = new AddressInfo(
                    address: jObject.address,
                    account: jObject.account,
                    amount: jObject.amount,
                    confirmations: jObject.confirmations
            )
            list << info
        }

        list
    }

    /**
     * Info about all received transactions by account
     *
     * @param minimumConfirmations is the minimum number of confirmations before payments are included
     * @param includeEmpty whether to include accounts that haven't received any payments
     * @return info about the received amount by account
     * @since 0.3.18
     */
    @Override
    List<AccountInfo> listReceivedByAccount(long minimumConfirmations, boolean includeEmpty) {
        JSONArray result = rpcOld('listreceivedbyaccount', [minimumConfirmations, includeEmpty])
        int size = result.length()

        List<AccountInfo> list = []

        for (int i = 0; i < size; i++) {
            def jObject = result.getJSONObject(i)
            AccountInfo info = new AccountInfo(
                    account: jObject.account,
                    amount: jObject.amount,
                    confirmations: jObject.confirmations
            )

            list << info
        }

        list
    }

    /**
     * Returns a list of at most <code>count</code> number of the last transactions for an account
     *
     * @param account the account related to the transactions, the default account if null or empty
     * @param count the maximum number of transactions returned, must be > 0
     * @return a list of at most <code>count</code> number of the last transactions for an account
     * @since 0.3.18
     */
    @Override
    List<TransactionInfo> listTransactions(String account = '', int count = 10) {
        if (count <= 0) {
            throw new IllegalArgumentException("count must be > 0")
        }

        JSONArray result = rpcOld('listtransactions', [account ?: '', count])
        int size = result.length()

        List<TransactionInfo> list = []

        for (int i = 0; i < size; i++) {
            JSONObject jObject = result.getJSONObject(i)
            TransactionInfo info = parseTransactionInfoFromJson(jObject)
            list << info
        }

        list
    }

    /**
     * Returns transaction information for a specific transaction ID
     *
     * @param txId the transaction ID
     * @return information about the transaction with that ID
     * @since 0.3.18
     */
    @Override
    TransactionInfo getTransaction(String txId) {
        def result = rpcOld('gettransaction', [txId])
        parseTransactionInfoFromJson(result)
    }

    private TransactionInfo parseTransactionInfoFromJson(JSONObject jObject) {
        TransactionInfo info = new TransactionInfo()

        info.setAmount(jObject.getDouble("amount"))

        if (!jObject.isNull("category")) {
            info.setCategory(jObject.getString("category"))
        }

        if (!jObject.isNull("fee")) {
            info.setFee(jObject.getDouble("fee"))
        }

        if (!jObject.isNull("message")) {
            info.setMessage(jObject.getString("message"))
        }

        if (!jObject.isNull("to")) {
            info.setTo(jObject.getString("to"))
        }

        if (!jObject.isNull("confirmations")) {
            info.setConfirmations(jObject.getLong("confirmations"))
        }

        if (!jObject.isNull("txid")) {
            info.setTxId(jObject.getString("txid"))
        }

        if (!jObject.isNull("otheraccount")) {
            info.setOtherAccount(jObject.getString("otheraccount"))
        }

        return info
    }

    /**
     * Return work information.
     *
     * @return work information
     */
    @Override
    WorkInfo getWork() {
        def result = rpc('getwork')

        new WorkInfo(
                midstate: result.midstate,
                data: result.data,
                hash1: result.hash1,
                target: result.target,
        )
    }

    /**
     * Tries to solve the block and returns true if it was successful
     *
     * @return true if the block was solved, false otherwise
     */
    @Override
    boolean getWork(String block) {
        rpc('getwork', [block])
    }

    /**
     * Sends amount from the server's available balance to bitcoinAddress.
     *
     * @param bitcoinAddress the bitcoinAddress to which we want to send bitcoins
     * @param amount the amount we wish to send, rounded to the nearest 0.01
     * @param comment a comment for this transfer, can be null
     * @param commentTo a comment to this transfer, can be null
     * @return the transaction ID for this transfer of Bitcoins
     */
    @Override
    String sendToAddress(String bitcoinAddress, double amount, String comment, String commentTo) {
        rpc('sendtoaddress', [bitcoinAddress, amount, comment, commentTo])
    }

    /**
     * Sends amount from account's balance to bitcoinAddress.
     * This method will fail if there is less than amount Bitcoins with minimumConfirmations
     * confirmations in the account's balance (unless account is the empty-string-named default
     * account it behaves like the #sendToAddress() method). Returns transaction ID on success.
     *
     * @param account the account we wish to send from, the default account if null or empty string
     * @param bitcoinAddress the address to which we want to send Bitcoins
     * @param amount the amount we wish to send, rounded to the nearest 0.01
     * @param minimumConfirmations minimum number of confirmations for a transaction to count
     * @param comment a comment for this transfer, can be null
     * @param commentTo a comment to this transfer, can be null
     * @return the transaction ID for this transfer of Bitcoins
     * @since 0.3.18
     */
    @Override
    String sendFrom(String account, String bitcoinAddress, double amount, int minimumConfirmations,
                    String comment, String commentTo) {
        if (minimumConfirmations <= 0) {
            throw new IllegalArgumentException("minimumConfirmations must be > 0")
        }

        rpc('sendfrom', [account ?: '', bitcoinAddress, amount, minimumConfirmations, comment, commentTo])
    }

    /**
     * Moves Bitcoins from one account to another on the same Bitcoin client.
     * This method will fail if there is less than amount Bitcoins with minimumConfirmations
     * confirmations in the fromAccount's balance. Returns transaction ID on success.
     *
     * @param fromAccount the account we wish to move from, the default account if null or empty string
     * @param toAccount the account we wish to move to, the default account if null or empty string
     * @param amount the amount we wish to move, rounded to the nearest 0.01
     * @param minimumConfirmations minimum number of confirmations for a transaction to count
     * @param comment a comment for this move, can be null
     * @return the transaction ID for this move of Bitcoins
     * @since 0.3.18
     */
    @Override
    boolean move(String fromAccount, String toAccount, double amount, int minimumConfirmations, String comment) {
        if (minimumConfirmations <= 0) {
            throw new IllegalArgumentException("minimumConfirmations must be > 0")
        }

        rpc("move", [fromAccount ?: '', toAccount ?: '', amount, minimumConfirmations, comment])
    }

    /**
     * Stops the bitcoin server
     */
    @Override
    void stop() {
        rpc('stop')
    }

    /**
     * Validates a Bitcoin address
     *
     * @param address the address we want to validate
     */
    @Override
    ValidatedAddressInfo validateAddress(String address) {
        def result = rpc('validateaddress', [address])

        ValidatedAddressInfo info = new ValidatedAddressInfo()

        info.with {
            isValid = result.isvalid
            if (isValid) {
                isMine = result.ismine
                info.address = result.address
            }
            if (isMine) {
                account = result.account
            }
        }

        info
    }

    /**
     * Copies the wallet.dat file to a backup destination
     *
     * @param destination the directory we wish to backup the wallet to
     */
    @Override
    void backupWallet(String destination) {
        rpc('backupwallet', [destination])
    }

    private String createRequest(String functionName, parameters = []) {
        def json = new JsonBuilder()

        json(
                jsonrpc: '2.0',
                id: idGenerator.random(),
                method: functionName,
                params: parameters,
        )

        json
    }

    private rpcOld(String functionName, parameters = []) {
        def message = createRequest(functionName, parameters)
        String response = session.sendAndReceive(message)
        def json = new JSONObject(response)

        json.result
    }

    private rpc(String functionName, parameters = []) {
        def message = createRequest(functionName, parameters)
        String response = session.sendAndReceive(message)
        def json = new JsonSlurper().parseText(response)

        json.result
    }
}
