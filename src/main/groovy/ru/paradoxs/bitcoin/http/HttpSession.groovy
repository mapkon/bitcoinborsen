/**
 * Copyright 2010 Aleksey Krivosheev (paradoxs.mail@gmail.com)
 *
 *    Licensed under the Apache License, Version 2.0 (the "License")
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package ru.paradoxs.bitcoin.http

import org.apache.commons.httpclient.auth.AuthScope
import org.json.JSONObject
import org.json.JSONTokener
import ru.paradoxs.bitcoin.http.exceptions.HttpSessionException
import org.apache.commons.httpclient.*
import org.apache.commons.httpclient.methods.*

/**
 * Manages the HTTP machinery for accessing the Bitcoin server.
 *
 * PLEASE NOTE that it doesn't do https, only http!
 */
class HttpSession implements Session {
    private static final JSON_CONTENT_TYPE = "application/json"
    private static final POST_CONTENT_TYPE = "text/plain"

    private final HttpClient client
    private final URI uri
    private final Credentials credentials

    HttpSession(URI uri, Credentials credentials) {
        this.uri = uri
        this.credentials = credentials

        client = new HttpClient()
        client.state.setCredentials(AuthScope.ANY, credentials)
    }

    String sendAndReceive(String message) {
        PostMethod method = new PostMethod(uri.toString())

        try {
            method.setRequestHeader("Content-Type", POST_CONTENT_TYPE)

            RequestEntity requestEntity = new StringRequestEntity(message, JSON_CONTENT_TYPE, null)
            method.setRequestEntity(requestEntity)

            client.executeMethod(method)
            int statusCode = method.getStatusCode()

            if (statusCode != HttpStatus.SC_OK) {
                throw new HttpSessionException("HTTP Status - ${HttpStatus.getStatusText(statusCode)} (${statusCode})")
            }

            JSONTokener tokener = new JSONTokener(method.getResponseBodyAsString())
            Object rawResponseMessage = tokener.nextValue()
            JSONObject response = (JSONObject) rawResponseMessage

            if (response == null) {
                throw new HttpSessionException("Invalid response type")
            }

            return response.toString()
        } finally {
            method.releaseConnection()
        }
    }
}
