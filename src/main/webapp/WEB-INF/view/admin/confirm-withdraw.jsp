<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n.frontend" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="title"/> - Confirm withdrawals</title>
    </head>
    <body>
        <h1>Confirm withdrawals</h1>
        <h2>Withdrawals that waits for confirmations</h2>        
        <table border="1">
            <tr>
                <th>Trader</th>
                <th>Amount</th>
                <th>Address</th>
                <th></th>
            </tr>
            <c:forEach items="${withdrawals}" var="withdrawal">
                <tr>
                    <td>${withdrawal.trader}</td>
                    <td>${withdrawal.amount}</td>
                    <td>${withdrawal.address}</td>
                    <td>
                        <form:form>
                            <input type="hidden" name="id" value="${withdrawal.id}"/>
                            <input type="submit" value="Confirm"/>
                        </form:form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
