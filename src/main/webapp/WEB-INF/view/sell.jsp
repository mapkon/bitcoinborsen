<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n.frontend" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="title"/> - <fmt:message key="sell-bitcoin"/></title>
    </head>
    <body>
        <h1><fmt:message key="place-order"/></h1>

        <form:form modelAttribute="newOrderForm" action="" method="post">
            <fieldset>
                <p>
                    <form:label for="price" path="price" cssErrorClass="error"><fmt:message key="bitcoin-price"/></form:label><br/>
                    <form:input path="price"/> <form:errors path="price"/>
                </p>

                <p>
                    <form:label for="amount" path="amount" cssErrorClass="error"><fmt:message key="bitcoin-amount"/></form:label><br/>
                    <form:input path="amount"/> <form:errors path="amount"/>
                </p>

                <p>
                    <input type="submit" value="<fmt:message key="sell-bitcoin"/>!"/>
                </p>
            </fieldset>
        </form:form>        
    </body>
</html>
