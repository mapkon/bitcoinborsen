<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n.frontend" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><fmt:message key="title"/> - <fmt:message key="deposit-bitcoin"/></title>
    </head>
    <body>
        <h1><fmt:message key="deposit-bitcoin"/></h1>
        <p>
            <fmt:message key="deposit-bitcoin-description"/>
        </p>
        <p>
            <fmt:message key="use-bitcoin-address"/>:
            <code>${trader.bitcoinAddress}</code>
        </p>
    </body>
</html>
